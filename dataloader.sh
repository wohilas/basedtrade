#!/usr/bin/env sh

SYMBOL=${1:-BTC_USDT}
TO=${2:-$(date +%s)}

set -e

STEP=1500
FROM=$(($TO - STEP * 60));

echo $STEP; echo $SYMBOL;

URL="/api/v1/market/candles?type=1min&symbol=${SYMBOL}&startAt=1566703297&endAt=1566789757"
